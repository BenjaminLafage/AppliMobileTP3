package com.example.tp3;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class TeamActivity extends AppCompatActivity {
    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    private JSONResponseHandlerEvent handlerEvent;
    private JSONResponseHandlerLeaderboard handlerLeaderboard;
    private JSONResponseHandlerTeam handlerTeam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);
        lastEvent = new Match();
        Intent intent= new Intent();
        team = (Team) getIntent().getParcelableExtra(Team.TAG);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);
        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Worker().execute();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(TeamActivity.this, MainActivity.class);
        intent.putExtra(Team.TAG, team);
        setResult(1, intent);
        finish();
        super.onBackPressed();
    }

    void updateView() {
        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());
        if(team.getTeamBadge() != null && !team.getTeamBadge().isEmpty()){
            new SetImageBadge().execute();
        }
    }

    public class Worker extends AsyncTask<TextView, String, Team> {

        @Override
        protected Team doInBackground(TextView... params) {
            boolean boucle = true;
            while(boucle) {
                try {
                    URL urlTeam = WebServiceUrl.buildSearchTeam(team.getName());
                    HttpURLConnection urlConnectionTeam = (HttpURLConnection) urlTeam.openConnection();
                    handlerTeam = new JSONResponseHandlerTeam(team);
                    try {
                        InputStream streamTeam = new BufferedInputStream(urlConnectionTeam.getInputStream());
                        handlerTeam.readJsonStream(streamTeam);
                        urlTeam = WebServiceUrl.buildGetRanking(team.getIdLeague());
                        urlConnectionTeam = (HttpURLConnection) urlTeam.openConnection();
                    } finally {
                        if (!handlerTeam.present){
                            this.cancel(true);
                            if(isCancelled()){
                                break;
                            }
                        }
                        urlConnectionTeam.disconnect();
                    }
                    try {
                        InputStream streamLeaderboard = new BufferedInputStream(urlConnectionTeam.getInputStream());
                        handlerLeaderboard = new JSONResponseHandlerLeaderboard(team);
                        handlerLeaderboard.readJsonStream(streamLeaderboard);
                        urlTeam = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                        urlConnectionTeam = (HttpURLConnection) urlTeam.openConnection();
                    } finally {
                        urlConnectionTeam.disconnect();
                    }
                    try {
                        InputStream streamEvent = new BufferedInputStream(urlConnectionTeam.getInputStream());
                        handlerEvent = new JSONResponseHandlerEvent(lastEvent, team);
                        handlerEvent.readJsonStream(streamEvent);
                    } finally {
                        urlConnectionTeam.disconnect();
                        boucle = false;
                    }
                } catch (MalformedURLException e) { //for buildSearchTeam
                    e.printStackTrace();
                } catch (IOException e) {   //for openConnection
                    e.printStackTrace();
                }
            }
            return team;
        }

        @Override
        protected void onPostExecute(Team team) {
            super.onPostExecute(team);
            updateView();
        }

    }

    private class SetImageBadge extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            try {
                URL urlTeam = new URL(team.getTeamBadge());
                HttpURLConnection urlConnectionBadge = (HttpURLConnection) urlTeam.openConnection();
                try {
                    InputStream streamBadge = new BufferedInputStream(urlConnectionBadge.getInputStream());
                    bitmap = BitmapFactory.decodeStream(streamBadge);
                } finally {
                    urlConnectionBadge.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imageBadge.setImageBitmap(bitmap);
        }
    }
}
