package com.example.tp3;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerLeaderboard {
    private Team team;
    private static final String TAG = JSONResponseHandlerLeaderboard.class.getSimpleName();

    public JSONResponseHandlerLeaderboard(Team team) {
        this.team = team;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readLeaderboards(reader);
        } finally {
            reader.close();
        }
    }

    public void readLeaderboards(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                readArrayLeaderboards(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayLeaderboards(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext() ) {
            reader.beginObject();
            while(reader.hasNext()) {
                String name = reader.nextName();
                if(name.equals("teamid")) {
                    long teamId = reader.nextLong();
                    if(teamId == team.getIdTeam()) {
                        team.setRanking(nb+1);
                    }
                    while(reader.hasNext()) {
                        String string = reader.nextName();
                        if(string.equals("total")) {
                            team.setTotalPoints(reader.nextInt());
                        }
                        else {
                            reader.skipValue();
                        }
                    }
                }
                else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }
}
