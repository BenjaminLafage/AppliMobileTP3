package com.example.tp3;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Parcelable;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView teamList;
    SportDbHelper sportDb;
    SimpleCursorAdapter adapter;
    static final int REQUEST_TEAM = 1;
    //RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sportDb = new SportDbHelper(this);
        if(sportDb.fetchAllTeams().getCount() < 1){
            sportDb.populate();
        }

        adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                sportDb.fetchAllTeams(),
                new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.
                        COLUMN_LEAGUE_NAME },
                new int[] { android.R.id.text1, android.R.id.text2});


        teamList = (ListView) findViewById(R.id.teamList);
        teamList.setAdapter(adapter);

        //TODO
        //Début de partie 4, non fonctionnelle
        /*recyclerView = (RecyclerView) findViewById(R.id.teamRecyclerView);
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipRefresh);
        ItemTouchHelper.Callback item = new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                int swipe = ItemTouchHelper.START | ItemTouchHelper.END;
                return makeMovementFlags(0,swipe);
            }

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int pos = viewHolder.getAdapterPosition();
                Team team = sportDb.getAllTeams().get(pos);
                sportDb.deleteTeam((int) team.getId());
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(item);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService((Context.CONNECTIVITY_SERVICE));
            if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED) {

            }
        });*/

        teamList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) parent.getItemAtPosition((position));
                Team teamSelected = sportDb.cursorToTeam(cursor);
                Intent intent = new Intent(MainActivity.this, TeamActivity.class);
                intent.putExtra(Team.TAG, (Parcelable) teamSelected);
                startActivityForResult(intent,REQUEST_TEAM);
            }
        });
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Team team = new Team("");
                Intent intent = new Intent(MainActivity.this, NewTeamActivity.class);
                intent.putExtra(Team.TAG, (Parcelable) team);
                intent.putExtra("add",true);
                startActivityForResult(intent, REQUEST_TEAM);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Team team;
        Boolean getBool, addTeam;
        if(intent != null) {
            team = intent.getParcelableExtra((Team.TAG));
            getBool = (intent.getExtras()).getBoolean("add");
            if(team != null) {
                if(getBool) {
                    addTeam = sportDb.addTeam(team);
                    if(addTeam) {
                        adapter.changeCursor(sportDb.fetchAllTeams());
                    }
                    else {
                        AlertDialog.Builder alert = new AlertDialog.Builder((MainActivity.this));
                        alert.setTitle("Ajout impossbile");
                        alert.setMessage("Cette équipe existe déjà");
                        alert.create();
                        alert.show();
                    }
                }
                else {
                    sportDb.updateTeam(team);
                }
            }
            adapter.changeCursor(sportDb.fetchAllTeams());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class updateTeam extends AsyncTask<TextView, String, List<Team>> {
        private List<Team> teamList = sportDb.getAllTeams();
        Boolean boucle = true;

        @Override
        protected List<Team> doInBackground(TextView... textViews) {
            for(Team team : teamList) {
                while (boucle) {
                    try {
                        URL urlTeam = WebServiceUrl.buildSearchTeam((team.getName()));
                        HttpURLConnection urlConnectionTeam = (HttpURLConnection) urlTeam.openConnection();
                        JSONResponseHandlerTeam handlerTeam = new JSONResponseHandlerTeam(team);
                        try {
                            InputStream streamTeam = new BufferedInputStream(urlConnectionTeam.getInputStream());
                            handlerTeam.readJsonStream(streamTeam);
                            urlTeam = WebServiceUrl.buildGetRanking(team.getIdLeague());
                            urlConnectionTeam = (HttpURLConnection) urlTeam.openConnection();
                        } finally {
                            urlConnectionTeam.disconnect();
                        }
                        try {
                            InputStream stream = new BufferedInputStream(urlConnectionTeam.getInputStream());
                            JSONResponseHandlerLeaderboard handlerLeaderboard = new JSONResponseHandlerLeaderboard(team);
                            handlerLeaderboard.readJsonStream(stream);
                            urlTeam = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                            urlConnectionTeam = (HttpURLConnection) urlTeam.openConnection();
                        } finally {
                            urlConnectionTeam.disconnect();
                        }
                        try {
                            InputStream stream = new BufferedInputStream(urlConnectionTeam.getInputStream());
                            Match event = new Match();
                            JSONResponseHandlerEvent handlerEvent = new JSONResponseHandlerEvent(event, team);
                            handlerEvent.readJsonStream(stream);
                        } finally {
                            urlConnectionTeam.disconnect();
                            boucle = false;
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return teamList;
        }
    }

    protected void onPostExecute(List<Team> teamList) {
        for(Team team : teamList) {
            sportDb.updateTeam(team);
        }
        adapter.changeCursor(sportDb.fetchAllTeams());
        adapter.notifyDataSetChanged();
    }
}
