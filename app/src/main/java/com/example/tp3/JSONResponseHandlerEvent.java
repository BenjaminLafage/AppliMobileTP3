package com.example.tp3;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerEvent {

    private Team team;
    private Match match;
    private static final String TAG = JSONResponseHandlerEvent.class.getSimpleName();


    public JSONResponseHandlerEvent(Match match, Team team){
        this.team = team;
        this.match = match;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readEvent(reader);
        } finally {
            reader.close();
        }
    }

    public void readEvent(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("results")) {
                readArrayEvent(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayEvent(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        Match match = new Match();
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb==0) {
                    if (name.equals("idEvent")) {
                        match.setId(reader.nextLong());
                    } else if (name.equals("strEvent")) {
                        match.setLabel(reader.nextString());
                    } else if (name.equals("strHomeTeam")) {
                        match.setHomeTeam(reader.nextString());
                    } else if (name.equals("strAwayTeam")) {
                        match.setAwayTeam(reader.nextString());
                    } else if (name.equals("intHomeScore")) {
                        if(reader.peek() != JsonToken.NULL){
                            match.setHomeScore(reader.nextInt());
                        }
                        else {
                            match.setHomeScore(-1);
                            reader.skipValue();
                        }
                    }
                    else if(name.equals("intAwayScore")) {
                        if(reader.peek() != JsonToken.NULL) {
                            match.setAwayScore(reader.nextInt());
                        }
                        else {
                            match.setAwayScore(-1);
                            reader.skipValue();
                        }
                    }
                        else {
                        reader.skipValue();
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        team.setLastEvent(match);
        reader.endArray();
    }
}
